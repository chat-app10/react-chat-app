type Environment = {
  API_URL: string;
};

export const environment: Environment = {
  API_URL: process.env.REACT_APP_API_URL || "",
};
