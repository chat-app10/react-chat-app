import { ChatPage } from "../modules/chat-page";
import { LayoutMain } from "../modules/layout";
import { routes } from "./routes";
import * as React from "react";
import { Route, Routes } from "react-router-dom";

export const RootRouter: React.FC = () => (
  <Routes>
    <Route element={<LayoutMain />}>
      <Route path={routes.home} element={<ChatPage />} />
    </Route>
  </Routes>
);
