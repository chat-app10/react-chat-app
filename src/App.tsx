import { RootRouter } from "./routes/root.router";
import * as React from "react";

function App() {
  return <RootRouter />;
}
export default App;
