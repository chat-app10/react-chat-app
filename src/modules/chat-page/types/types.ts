export type ResponseMessage = {
  author: string | null;
  message: string;
  datetime: string;
};

export type ReceivedMessage = {
  id: number;
  author: string;
  message: string;
  datetime: string;
};
