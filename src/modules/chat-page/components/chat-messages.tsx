import { UseGetMessages } from "../hooks";
import { ReceivedMessage } from "../types/types";
import { ChatMessage } from "./chat-message";
import { Paper, Stack } from "@mui/material";
import * as React from "react";

export const ChatMessages: React.FC = () => {
  const messages = UseGetMessages();
  return (
    <Paper
      sx={{
        width: "fullWidth",
        height: "550px",
        overflowY: "auto",
        padding: "5px",
        margin: "30px 0",
      }}
    >
      <Stack>
        {messages.data?.map((message: ReceivedMessage) => (
          <ChatMessage message={message} />
        ))}
      </Stack>
    </Paper>
  );
};
