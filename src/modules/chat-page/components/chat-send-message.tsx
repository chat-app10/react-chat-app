import { UseGetCurrentDatetime, UseGetMessages } from "../hooks";
import { ResponseMessage } from "../types/types";
import { Button, Stack, TextField, Typography } from "@mui/material";
import axios from "axios";
import * as React from "react";
import { useState } from "react";

export const ChatSendMessage: React.FC = () => {
  const author: string | null = localStorage.getItem("name");
  const [message, setMessage] = useState<string>("");
  const handleChangeMessage = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setMessage(e.target.value);
  };

  const datetime = UseGetCurrentDatetime();

  const response: ResponseMessage = {
    author: author,
    message: message,
    datetime: datetime,
  };

  const handleSendMessage = () => {
    axios.post("http://localhost:3005/newmessage", response);
    UseGetMessages();
    setMessage("");
  };

  return (
    <Stack
      direction="column"
      justifyContent="center"
      alignItems="flex-start"
      spacing={2}
    >
      <Typography sx={{ color: "#aed9e0" }}>{author}</Typography>
      <TextField
        fullWidth
        id="outlined-multiline-static"
        label="Write message"
        multiline
        rows={5}
        value={message}
        onChange={handleChangeMessage}
      />
      <Button variant="contained" onClick={handleSendMessage}>
        Send message
      </Button>
    </Stack>
  );
};
