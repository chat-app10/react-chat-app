import { ReceivedMessage } from "../types/types";
import { Stack, Typography } from "@mui/material";
import * as React from "react";

type Props = {
  message: ReceivedMessage;
};
export const ChatMessage: React.FC<Props> = (props) => {
  const { message } = props;
  return (
    <Stack>
      <Typography sx={{ margin: "10px 0 0 0", color: "gray" }}>
        {message.datetime}
      </Typography>
      <Typography
        sx={{ margin: "0 0 3px 0", color: "#74c69d" }}
        variant="h6"
        component="h3"
      >
        {message.author}
      </Typography>
      <Typography sx={{ margin: " 0 0 10px 0 " }}>{message.message}</Typography>
    </Stack>
  );
};
