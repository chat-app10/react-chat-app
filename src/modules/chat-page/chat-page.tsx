import { ChatMessages, ChatSendMessage } from "./components";
import { Button, Stack } from "@mui/material";
import * as React from "react";

export const ChatPage: React.FC = () => {
  const handleRemoveName = () => {
    localStorage.removeItem("name");
  };
  return (
    <Stack>
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="baseline"
        spacing={2}
      >
        <h1>Chat page</h1>
        <Button onClick={handleRemoveName} variant="outlined">
          Logout
        </Button>
      </Stack>
      <ChatMessages />
      <ChatSendMessage />
    </Stack>
  );
};
