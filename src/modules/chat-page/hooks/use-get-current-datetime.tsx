export const UseGetCurrentDatetime = () => {
  const datetime = new Date();
  const day =
    datetime.getDay() < 10 ? `0${datetime.getDay()}` : datetime.getDay();
  const month =
    datetime.getMonth() + 1 < 10
      ? `0${datetime.getMonth() + 1}`
      : datetime.getMonth() + 1;
  const year = datetime.getFullYear();
  const hour =
    datetime.getHours() < 10 ? `0${datetime.getHours()}` : datetime.getHours();
  const minute =
    datetime.getMinutes() < 10
      ? `0${datetime.getMinutes()}`
      : datetime.getMinutes();
  return `${year}-${month}-${day}  ${hour}:${minute}`;
};
