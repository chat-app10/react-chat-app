import { httpClient } from "../../../common/axios";
import { ReceivedMessage } from "../types/types";
import { useQuery, UseQueryResult } from "react-query";

export const UseGetMessages = (): UseQueryResult<ReceivedMessage[]> => {
  return useQuery<ReceivedMessage[]>(["message"], async () => {
    const response = await httpClient.get(`/messages`);
    return response.data;
  });
};
