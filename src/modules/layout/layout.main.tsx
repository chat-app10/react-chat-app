import { LoginPage } from "../login-page";
import { Container } from "@mui/material";
import * as React from "react";
import { Outlet } from "react-router-dom";

export const LayoutMain: React.FC = () => {
  const isTrue = true;

  return (
    <>
      <Container maxWidth="xl" sx={{ marginTop: 0, marginBottom: 15 }}>
        {localStorage.getItem("name") ? <Outlet /> : <LoginPage />}
      </Container>
    </>
  );
};
