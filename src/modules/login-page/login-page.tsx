import { Box, Button, Stack, TextField, Typography } from "@mui/material";
import * as React from "react";
import { useState } from "react";

export const LoginPage: React.FC = () => {
  const [name, setName] = useState<string>("");
  const handleChangeName = (e: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setName(e.target.value);
  };
  return (
    <Stack
      direction="column"
      justifyContent="center"
      alignItems="center"
      spacing={2}
    >
      <Typography variant="h3" component="h1">
        Welcome! I see you haven`t been there yet.
      </Typography>
      <Box
        sx={{
          width: "200px",
        }}
      >
        <TextField
          fullWidth
          id="outlined-basic"
          label="Name"
          variant="outlined"
          value={name}
          onChange={handleChangeName}
        />
        <Button
          fullWidth
          onClick={() => localStorage.setItem("name", name)}
          disabled={name == ""}
          variant="contained"
          sx={{
            mt: 2,
          }}
        >
          Go ahead
        </Button>
      </Box>
    </Stack>
  );
};
